#!/usr/bin/env bash

# Function to generate an SSH key pair and print the public key
generate_ssh_key() {
  local email=$1
  ssh-keygen -t ed25519 -C "${email}" -f ~/.ssh/id_ed25519 -N "" # No passphrase
  cat ~/.ssh/id_ed25519.pub # Output the public SSH key
}

# Function to generate a GPG key and print the public key
generate_gpg_key() {
  gpg --full-generate-key
  local gpg_key_id=$(gpg --list-secret-keys --keyid-format LONG | grep sec | awk '{print $2}' | cut -d'/' -f2)
  gpg --armor --export "${gpg_key_id}" # Correctly output the public GPG key
}

# Main function to orchestrate key generation and output formatting
main() {
  local email="benjaminshawki@gmail.com"
  
  # Generate and capture the SSH public key
  local ssh_key=$(generate_ssh_key "${email}")
  
  # Generate and capture the GPG public key
  local gpg_key=$(generate_gpg_key)
  
  # Print both keys separated by a line
  echo -e "${ssh_key}\n---\n${gpg_key}"
}

main
